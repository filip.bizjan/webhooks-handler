require("dotenv").config()
let {
    exec
} = require("child_process")
let http = require('http')
let createHandler = require('node-gitlab-webhook')
let handler = createHandler([{
    path: '/api/gitlab/webhooks',
    secret: process.env.SECRET
}, ])

http.createServer(function(req, res) {
    handler(req, res, function(err) {
        res.statusCode = 404
        res.end('no such location')
    })
}).listen(8080)

handler.on('error', function(err) {
    console.error('Error:', err.message)
})

const commandLogin = `sudo docker login -u ${process.env.DOCKER_USERNAME} -p ${process.env.DOCKER_PASSWORD}`
const commandPull = `sudo docker pull ${process.env.DOCKER_USERNAME}/${process.env.DOCKER_IMAGE_NAME}:latest`
const commandStop = `sudo docker stop elektricni-daljnovodi-eles || true && sudo docker rm elektricni-daljnovodi-eles || true`
const commandRestart = `sudo docker run --privileged -p 3000:3000 -d --name elektricni-daljnovodi-eles ${process.env.DOCKER_USERNAME}/${process.env.DOCKER_IMAGE_NAME}:latest`

function execute(event){
    console.log(
        'Received an event for %s to %s',
        event.payload.repository.name,
        event.payload.ref
    )
    switch (event.path) {
        case '/api/gitlab/webhooks':
            exec(commandLogin, (err, stdout, stderr) => {
                if (err) {
                    console.error(`Error logging into docker hub: ${err}`);
                    return;
                }
                console.log(`Logging in: ${stdout}`)

                exec(commandPull, (err1, stdout1, stderr1) => {
                    if (err1) {
                        console.log(`Error pulling image: ${err1}`)
                        return;
                    }
                    console.log(`Pulling: ${stdout1}`)

                    exec(commandStop, (err3, stdout3, stderr3) => {
                        if (err3) console.log(`Error stopping: ${err3}`)

                        exec(commandRestart, (err2, stdout2, stderr2) => {
                            if (err2) console.log(`Restart error: ${err2}`)
                            console.log(`Restarting: ${stdout2}`)
                        })
                    })
                });
            });
        break
    }
}

handler.on('push', function(event) {
    execute(event)
})

handler.on('job', function(event) {
    execute(event)
})

handler.on('pipeline', function(event) {
    execute(event)
})
